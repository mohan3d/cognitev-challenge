from rest_framework import serializers

from core.models import User, Token, Status


class UserSerializer(serializers.ModelSerializer):
    """
    Serializes User object into different formats (json and xml)
    and json/xml into User object.
    """

    class Meta:
        model = User

        fields = ('id', 'first_name', 'last_name', 'password', 'country_code',
                  'phone_number', 'gender', 'birthdate', 'avatar', 'email')

        extra_kwargs = {
            'password': {'write_only': True}
        }


class TokenSerializer(serializers.Serializer):
    """Validates required data (password/phone_number) before generating auth-token."""

    phone_number = serializers.CharField()
    password = serializers.CharField(
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        phone_number = attrs.get('phone_number')
        password = attrs.get('password')

        if phone_number and password:
            try:
                user = User.objects.get(phone_number=phone_number, password=password)
            except User.DoesNotExist:
                msg = 'Unable to log in with provided credentials.'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Must include "phone_number" and "password".'
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class StatusSerializer(serializers.ModelSerializer):
    """
    Validates required data (auth-token/phone_number)
    and serializes Status object.
    """

    phone_number = serializers.CharField(write_only=True)
    auth_token = serializers.CharField(write_only=True)

    def validate(self, attrs):
        phone_number = attrs.get('phone_number')
        auth_token = attrs.get('auth_token')

        if phone_number and auth_token:
            try:
                user = User.objects.get(phone_number=phone_number)
                token = Token.objects.get(key=auth_token)
                assert token.user.pk == user.pk

            except (AssertionError, User.DoesNotExist, Token.DoesNotExist):
                msg = "phone_number/auth_token combination doesn't match our records."
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Must include "phone_number" and "auth_token".'
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        attrs.pop('phone_number')
        attrs.pop('auth_token')
        return attrs

    class Meta:
        model = Status
        fields = ('id', 'status', 'phone_number', 'auth_token')
