from django.urls import path

from .views import UserCreate, ObtainAuthToken, StatusCreate

urlpatterns = [
    path('users/', UserCreate.as_view(), name='users-create'),
    path('auth_token/', ObtainAuthToken.as_view(), name='obtain_auth_token'),
    path('status/', StatusCreate.as_view(), name='status-create'),
]
