from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import User, Token, Status
from .serializers import UserSerializer, TokenSerializer, StatusSerializer


class UserCreate(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class StatusCreate(generics.CreateAPIView):
    queryset = Status.objects.all()
    serializer_class = StatusSerializer


class ObtainAuthToken(APIView):
    serializer_class = TokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, _ = Token.objects.get_or_create(user=user)
        return Response({
            'auth-token': token.key,
            'user_id': user.pk,
            'phone_number': str(user.phone_number)
        })
