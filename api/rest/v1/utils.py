from rest_framework.views import exception_handler


def format_exception(data):
    """Changes exception format to the desired one."""

    def format_entry(value):
        if type(value) is list:
            return [{'error': error} for error in value]
        return {'error': value}

    return {
        key: format_entry(value)
        for key, value in data.items()
    }


def custom_exception_handler(exc, context):
    """
    Custom exception handler for Django Rest Framework that alters
    exception format.
    """
    response = exception_handler(exc, context)

    if response is not None:
        response.data = {
            'errors': format_exception(response.data)
        }

    return response
