import binascii
import os


def generate_key():
    """Return the hexadecimal representation of twenty character length bytes."""
    return binascii.hexlify(os.urandom(20)).decode()
