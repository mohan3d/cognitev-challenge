# Generated by Django 2.0 on 2018-08-15 16:00

import core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Token',
            fields=[
                ('key', models.CharField(max_length=40, primary_key=True, serialize=False)),
            ],
        ),
        migrations.AlterField(
            model_name='user',
            name='birthdate',
            field=models.DateField(validators=[core.validators.validate_not_in_future]),
        ),
        migrations.AddField(
            model_name='token',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='auth_token', to='core.User'),
        ),
    ]
