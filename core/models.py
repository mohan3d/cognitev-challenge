from django.conf import settings
from django.core import validators
from django.db import models
from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField

from .utils import generate_key
from .validators import validate_not_in_future


class User(models.Model):
    """Stores a single User entry."""

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    first_name = models.CharField(max_length=settings.NAME_MAX_LENGTH)
    last_name = models.CharField(max_length=settings.NAME_MAX_LENGTH)

    password = models.CharField(max_length=settings.PASSWORD_MAX_LENGTH, validators=[
        validators.MinLengthValidator(settings.PASSWORD_MIN_LENGTH)
    ])

    country_code = CountryField()
    phone_number = PhoneNumberField(unique=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)

    birthdate = models.DateField(validators=[
        validate_not_in_future
    ])

    avatar = models.FileField(validators=[
        validators.FileExtensionValidator(allowed_extensions=settings.AVATAR_ALLOWED_EXTENSIONS)
    ])

    email = models.EmailField(blank=True)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class Token(models.Model):
    """Stores automatically generated auth-key related to User object."""

    key = models.CharField(max_length=settings.AUTH_TOKEN_MAX_LENGTH, primary_key=True)

    user = models.OneToOneField(
        User, related_name='auth_token', on_delete=models.CASCADE
    )

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = generate_key()
        return super(Token, self).save(*args, **kwargs)

    def __str__(self):
        return self.key


class Status(models.Model):
    """Stores status related to User object."""

    status = models.TextField()
    user = models.ForeignKey(
        User, related_name='status', on_delete=models.CASCADE
    )

    def __str__(self):
        return self.status
