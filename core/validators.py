import datetime

from django.core.exceptions import ValidationError


def validate_not_in_future(value):
    """Raises validation error if given date is greater than today"""

    if value > datetime.date.today():
        raise ValidationError('The birthdate is in the future')
