Cognitev Challenge
==================

A restful api written in `python`_ and `django`_ contains three endpoints.


Install Dependencies
--------------------

.. code-block:: bash

    $ pip install -r requirements.txt


Deployment
----------

.. code:: bash

    $ python manage.py migrate


Development server
------------------

.. code:: bash

    $ python manage.py runserver


Endpoints
---------

.. code::

    http://localhost:8000/api/v1/users/         # Create User API
    http://localhost:8000/api/v1/auth_token/    # Generate Auth-Token API
    http://localhost:8000/api/v1/status/        # Create Status API


Project structure
-----------------
::

    .
    ├── api                             # api application contains everything related to api
    │   └── rest                        # all restful APIs versions will live here
    │       └── v1                      # api version 1
    │           ├── serializers.py      # contains classes that serialize objects to json/xml and vice versa
    │           ├── urls.py             # api version 1 exposed urls
    │           ├── utils.py            # custom exception handler
    │           └── views.py            # views to process requests
    │
    ├── core                            # contains models and related logic.
    │   ├── migrations                  # models migrations related to all models in core app
    │   ├── apps.py                     # entry point for django project (used in core.settings)
    │   ├── models.py                   # contains all models (User, Token and Status models)
    │   ├── utils.py                    # utility functions
    │   └── validators.py               # custom model validators
    │
    ├── cognitev                        # root application.
    │   ├── settings.py                 # contains settings and configurations for all applications
    │   ├── urls.py                     # public accessible urls
    │   └── wsgi.py                     # entry point for wsgi
    │
    ├── requirements.txt                # project dependencies
    ├── manage.py                       # entry point for django cli commands
    └── README.rst

.. _python: https://www.python.org/
.. _django: https://www.djangoproject.com/